package com.kemahti.kemahti;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;

public class MahasiswaLogin extends AppCompatActivity {

    Button login;
    EditText txt1, txt2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_login);

        TextView loginMahasiswa = (TextView) findViewById(R.id.textView4);
        loginMahasiswa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MahasiswaDaftar.class);
                startActivity(i);
            }
        });
        login = (Button)findViewById(R.id.btnLogin);
        txt1 = (EditText)findViewById(R.id.username);
        txt2 = (EditText) findViewById(R.id.password);

        /*
        login.setOnClickListener(new View.OnClickListener(){
            @Override

            public void onClick (View v)
            {
                if (txt1.getText().toString().equals("admin") && txt2.getText().toString().equals("admin")) {
                    Intent a = new Intent(getApplicationContext(), MahasiswaHome.class);
                    startActivity(a);
                } else {
                    Toast.makeText(getApplicationContext(), "Ignore U", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
        };*/

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mahasiswa_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void Admin(View view){
        Intent Admin = new Intent(this, Admin.class);
        startActivity(Admin);
    }
}
