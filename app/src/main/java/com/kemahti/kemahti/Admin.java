package com.kemahti.kemahti;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class Admin extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void AdminDetailKegiatan(View view){
        Intent AdminDetailKegiatan = new Intent(this, AdminDetailKegiatan.class);
        startActivity(AdminDetailKegiatan);
    }

    public void AdminDetailDosen(View view){
        Intent AdminDetailDosen = new Intent(this, AdminDetailDosen.class);
        startActivity(AdminDetailDosen);
    }

    public void AdminDataMahasiswa(View view){
        Intent AdminDataMahasiswa = new Intent(this, AdminDataMahasiswa.class);
        startActivity(AdminDataMahasiswa);
    }

    /*
    public void AdminDetailKegiatan(){
        Intent detail_kegiatan = new Intent(com.kemahti.kemahti.AdminDetailKegiatan);
        startActivity(detail_kegiatan);
    }
    */
}
