package com.kemahti.kemahti;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;


public class Login extends AppCompatActivity {

    //Button linkloginmhs;
    //Button linklogindosen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        /*
        Button loginmahasiswa = (Button) findViewById(R.id.linkloginmhs);

        loginmahasiswa.setOnClickListener(new View.OnClickListener() {
        //loginmahasiswa.setOnClickListener(new View.onClickListener() {

            public void onClick(View view) {
                Intent mahasiswa_login = new Intent(Login.this, MahasiswaLogin.class);
                startActivity(mahasiswa_login);
                finish();
            }

        });
        */

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void mahasiswaLogin(View view){
        Intent mahasiswalogin = new Intent(this, MahasiswaLogin.class);
        startActivity(mahasiswalogin);
    }

    public void dosenLogin(View view){
        Intent dosenlogin = new Intent(this, DosenLogin.class);
        startActivity(dosenlogin);
    }
}
